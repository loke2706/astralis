const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    Email: { type: String, unique: true, required: true },
    Name: { type: String, required: true },
    Username: { type: String, unique: true, required: true },
    Mobile: { type: String },
    Password: { type: String, required: true },
    created: { type: Date, default: Date.now },
    updated: Date
});

schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        // remove these props when object is serialized
        delete ret._id;
        delete ret.Password;
    }
});

module.exports = mongoose.model('Account', schema);