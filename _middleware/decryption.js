const crypto = require("crypto");

const algorithm = "aes-256-cbc";

const password = "dSgUkXp2s5v8y/BE(H+MbQeThWmYq3t";

module.exports = decrypt;

function decrypt(encryptedData) {
    var decipher = crypto.createDecipher(algorithm, password)
    var decryptedData = decipher.update(encryptedData, 'hex', 'utf8')
    decryptedData += decipher.final('utf8');
    return decryptedData;
}