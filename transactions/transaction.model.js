const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    ID: { type: String, unique: true, required: true },
    userId: { type: String, required: true },
    AccountName: { type: String, required: true },
    AccountNo: { type: String, required: true },
    BankIFSC: { type: String, required: true },
    Amount: { type: String, required: true },
    Value: { type: String, required: true },
    Remarks: { type: String, required: true },
    created: { type: Date, default: Date.now },
    updated: Date
});

schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        // remove these props when object is serialized
        delete ret._id;
    }
});

module.exports = mongoose.model('Transaction', schema);