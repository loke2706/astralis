const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const transactionService = require('./transaction.service');

// routes
router.post('/createTransaction', authorize(), createTransactionSchema, createTransaction);
router.get('/', authorize(), getAll);
router.get('/getTransactionByUserId/:id', authorize(), getbyuserid);
router.post('/getTransactionByTransactionId', authorize(), getTransactionSchema, getbytransId);
router.put('/:id', authorize(), update);
router.delete('/:id', authorize(), _delete);

module.exports = router;

function createTransactionSchema(req, res, next) {
    const schema = Joi.object({
        ID: Joi.string().required(),
        userId: Joi.string().required(),
        AccountName: Joi.string().required(),
        AccountNo: Joi.string().required(),
        BankIFSC: Joi.string().required(),
        Amount: Joi.string().required(),
        Value: Joi.string().required(),
        Remarks: Joi.string().required(),
    });
    validateRequest(req, next, schema);
}

function createTransaction(req, res, next) {
    transactionService.createTransaction(req.body)
        .then(() => res.json({ message: 'Transaction Successful.' }))
        .catch(next);
}

function getAll(req, res, next) {
    transactionService.getAll()
        .then(transactions => res.json(transactions))
        .catch(next);
}

function getbyuserid(req, res, next) {
    if (req.params.id !== req.user.id) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    transactionService.getbyuserid(req.params.id)
        .then(transaction => transaction ? res.json(transaction) : res.sendStatus(404))
        .catch(next);
}

function getTransactionSchema(req, res, next) {
    const schema = Joi.object({
        ID: Joi.string().required(),
    });
    validateRequest(req, next, schema);
}

function getbytransId(req, res, next) {
    transactionService.getbytransId(req.body.ID)
        .then(transaction => transaction ? res.json(transaction) : res.sendStatus(404))
        .catch(next);
}

function update(req, res, next) {
    transactionService.updatetransactionbyid(req.params.id, req.body)
        .then(account => res.json({ message: "Updated successfully" }))
        .catch(next);
}

function _delete(req, res, next) {
    transactionService._delete(req.params.id)
        .then(() => res.json({ message: 'Transaction deleted successfully' }))
        .catch(next);
}