﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const accountService = require('./account.service');

// routes
router.post('/authenticate', authenticateSchema, authenticate);
router.post('/register', registerSchema, register);
router.get('/', authorize(), getAll);
router.get('/:id', authorize(), getById);

module.exports = router;

function authenticateSchema(req, res, next) {
    const schema = Joi.object({
        Email: Joi.string().required(),
        Password: Joi.string().required()
    });
    validateRequest(req, next, schema);
}

function authenticate(req, res, next) {
    const { Email, Password } = req.body;
    accountService.authenticate({ Email, Password })
        .then(({...account }) => {
            res.json(account);
        })
        .catch(next);
}

function registerSchema(req, res, next) {
    const schema = Joi.object({
        Name: Joi.string().required(),
        Email: Joi.string().email().required(),
        Mobile: Joi.string().required(),
        Username: Joi.string().required(),
        Password: Joi.string().min(6).required(),
        confirmPassword: Joi.string().valid(Joi.ref('Password')).required(),
    });
    validateRequest(req, next, schema);
}

function register(req, res, next) {
    accountService.register(req.body)
        .then(() => res.json({ message: 'Registration successful.' }))
        .catch(next);
}

function getAll(req, res, next) {
    accountService.getAll()
        .then(accounts => res.json(accounts))
        .catch(next);
}

function getById(req, res, next) {
    if (req.params.id !== req.user.id) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    accountService.getById(req.params.id)
        .then(account => account ? res.json(account) : res.sendStatus(404))
        .catch(next);
}