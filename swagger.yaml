openapi: 3.0.0
info:
  title: Node.js Sign-up and Verification API
  description: Node.js + MongoDB - API with email sign-up, authentication
  version: 1.0.0

servers:
  - url: http://localhost:4000
    description: Local development server

paths:
  /accounts/authenticate:
    post:
      summary: Authenticate account credentials and return a JWT token
      description: The required fields has to be entered for authenticating.
      operationId: authenticate
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                  example: "jason@example.com"
                password:
                  type: string
                  example: "pass123"
              required:
                - email
                - password
      responses:
        "200":
          description: Account details, a JWT access token.
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: string
                    example: "5eb12e197e06a76ccdefc121"
                  Name:
                    type: string
                    example: "Jason Watmore"
                  Email:
                    type: string
                    example: "jason@example.com"
                  Mobile:
                    type: string
                    example: "9999999999"
                  Username:
                    type: string
                    example: "user123"
                  created:
                    type: string
                    example: "2020-05-05T09:12:57.848Z"
                  jwtToken:
                    type: string
                    example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1ZWIxMmUxOTdlMDZhNzZjY2RlZmMxMjEiLCJpZCI6IjVlYjEyZTE5N2UwNmE3NmNjZGVmYzEyMSIsImlhdCI6MTU4ODc1ODE1N30.xR9H0STbFOpSkuGA9jHNZOJ6eS7umHHqKRhI807YT1Y"
        "400":
          description: The email or password is incorrect
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Email or password is incorrect"
  /accounts/register:
    post:
      summary: Register a new user account
      description: The required fields must be given to create the user account.
      operationId: register
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                Name:
                  type: string
                  example: "Jason Watmore"
                Email:
                  type: string
                  example: "jason@example.com"
                Mobile:
                  type: string
                  example: "9999999999"
                Username:
                  type: string
                  example: "user123"
                Password:
                  type: string
                  example: "pass123"
                confirmPassword:
                  type: string
                  example: "pass123"
              required:
                - Name
                - Email
                - Mobile
                - Username
                - Password
                - confirmPassword
      responses:
        "200":
          description: The registration request was successful.
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Registration successful."
  /accounts:
    get:
      summary: Get a list of all accounts
      description: display  all the user accounts.
      operationId: getAllAccounts
      security:
        - bearerAuth: []
      responses:
        "200":
          description: An array of all accounts
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: string
                      example: "5eb12e197e06a76ccdefc121"
                    Name:
                      type: string
                      example: "Jason Watmore"
                    Email:
                      type: string
                      example: "jason@example.com"
                    Mobile:
                      type: string
                      example: "9999999999"
                    Username:
                      type: string
                      example: "user123"
                    created:
                      type: string
                      example: "2020-05-05T09:12:57.848Z"
                    updated:
                      type: string
                      example: "2020-05-08T03:11:21.553Z"
        "401":
          $ref: "#/components/responses/UnauthorizedError"
  /accounts/:id:
    get:
      summary: Get single user Account
      description: display user accounts based on user Id.
      operationId: Get single user Account
      security:
        - bearerAuth: []
      responses:
        "200":
          description: Object of user account details
          content:
            application/json:
              schema:
                items:
                  type: object
                  properties:
                    id:
                      type: string
                      example: "5eb12e197e06a76ccdefc121"
                    Name:
                      type: string
                      example: "Jason Watmore"
                    Email:
                      type: string
                      example: "jason@example.com"
                    Mobile:
                      type: string
                      example: "9999999999"
                    Username:
                      type: string
                      example: "user123"
                    created:
                      type: string
                      example: "2020-05-05T09:12:57.848Z"
                    updated:
                      type: string
                      example: "2020-05-08T03:11:21.553Z"
        "401":
          $ref: "#/components/responses/UnauthorizedError"        
  /transactions/createTransaction:
    post:
      summary: Create a New Transactions
      description: The required fields must be given to create the transactions and also Bearer token is needed.
      operationId: createTransaction
      security:
        - bearerAuth: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                ID:
                  type: string
                  example: "#1708031724431131"
                userId:
                  type: string
                  example: "611a9a8f7e492c2a48d6d6f5"
                AccountName:
                  type: string
                  example: "Jason Watmore"
                AccountNo:
                  type: string
                  example: "00638015"
                BankIFSC:
                  type: string
                  example: "TESt12354"
                Amount:
                  type: string
                  example: "1000"
                Remarks:
                  type: string
                  example: "Collecting credit card rewards? You'll earn applicable points with this transaction."
              required:
                - ID
                - userId
                - AccountName
                - AccountNo
                - BankIFSC
                - Amount
                - Value
                - Remarks
      responses:
        "200":
          description: Transaction successful.
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Transaction successful."
  /transactions:
    get:
      summary: Get a list of all Transactions
      description: display  all the user Transactions.
      operationId: getAllTransactions
      security:
        - bearerAuth: []
      responses:
        "200":
          description: An array of all Transactions
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: string
                      example: "5eb12e197e06a76ccdefc121"
                    ID:
                      type: string
                      example: "#1708031724431131"
                    userId:
                      type: string
                      example: "611a9a8f7e492c2a48d6d6f5"
                    AccountName:
                      type: string
                      example: "Jason Watmore"
                    AccountNo:
                      type: string
                      example: "00638015"
                    BankIFSC:
                      type: string
                      example: "TESt12354"
                    Amount:
                      type: string
                      example: "1000"
                    Remarks:
                      type: string
                      example: "Collecting credit card rewards? You'll earn applicable points with this transaction."
                    created:
                      type: string
                      example: "2020-05-08T03:11:21.553Z"  
        "401":
          $ref: "#/components/responses/UnauthorizedError"
  /transactions/getTransactionByUserId/:id:
    get:
      summary: Get a list of all Transactions made by a user
      description: display  all the user Transactions.
      operationId: getAllTransactions done by a user
      security:
        - bearerAuth: []
      responses:
        "200":
          description: An array of all Transactions made by user
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: string
                      example: "5eb12e197e06a76ccdefc121"
                    ID:
                      type: string
                      example: "#1708031724431131"
                    userId:
                      type: string
                      example: "611a9a8f7e492c2a48d6d6f5"
                    AccountName:
                      type: string
                      example: "Jason Watmore"
                    AccountNo:
                      type: string
                      example: "00638015"
                    BankIFSC:
                      type: string
                      example: "TESt12354"
                    Amount:
                      type: string
                      example: "1000"
                    Remarks:
                      type: string
                      example: "Collecting credit card rewards? You'll earn applicable points with this transaction."
                    created:
                      type: string
                      example: "2020-05-08T03:11:21.553Z"  
        "401":
          $ref: "#/components/responses/UnauthorizedError" 
  /transactions/getTransactionByTransactionId:
    post:
      summary: Get a list of all Transactions by Transaction ID
      description: display Transactions by Transaction ID.
      operationId: display Transactions by Transaction ID
      security:
        - bearerAuth: []
      responses:
        "200":
          description: Object of Tansactions based on ID
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: string
                      example: "5eb12e197e06a76ccdefc121"
                    ID:
                      type: string
                      example: "#1708031724431131"
                    userId:
                      type: string
                      example: "611a9a8f7e492c2a48d6d6f5"
                    AccountName:
                      type: string
                      example: "Jason Watmore"
                    AccountNo:
                      type: string
                      example: "00638015"
                    BankIFSC:
                      type: string
                      example: "TESt12354"
                    Amount:
                      type: string
                      example: "1000"
                    Remarks:
                      type: string
                      example: "Collecting credit card rewards? You'll earn applicable points with this transaction."
                    created:
                      type: string
                      example: "2020-05-08T03:11:21.553Z"  
        "401":
          $ref: "#/components/responses/UnauthorizedError"   
  /transactions/:id:
    put:
      summary: update Transactions by Transaction ID
      description: update Transactions by Transaction ID.
      operationId: update Transactions by Transaction ID
      security:
        - bearerAuth: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                AccountName:
                  type: string
                  example: "Jason Watmore"
                AccountNo:
                  type: string
                  example: "00638015"
                BankIFSC:
                  type: string
                  example: "TESt12354"
                Amount:
                  type: string
                  example: "1000"
                Remarks:
                  type: string
                  example: "Collecting credit card rewards? You'll earn applicable points with this transaction."
              required:
                - AccountName
                - AccountNo
                - BankIFSC
                - Amount
                - Value
                - Remarks  
      responses:
        "200":
          description: Transaction Updated successfully.
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Updated successfully."
        "401":
          $ref: "#/components/responses/UnauthorizedError" 
          
  /transactions/deletetransactions/:id:
    delete:
      summary: Delete Transactions by Transaction ID
      description: Delete Transactions by Transaction ID.
      operationId: Delete Transactions by Transaction ID
      security:
        - bearerAuth: []
      responses:
        "200":
          description: Transaction Deleted successfully.
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Deleted successfully."
        "401":
          $ref: "#/components/responses/UnauthorizedError"                
          
components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
  responses:
    UnauthorizedError:
      description: Access token is missing or invalid, or the user does not have access to perform the action
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                type: string
                example: "Unauthorized"
    NotFoundError:
      description: Not Found
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                type: string
                example: "Not Found"