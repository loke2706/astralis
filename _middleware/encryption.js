const crypto = require("crypto");

const algorithm = "aes-256-cbc";

const password = "dSgUkXp2s5v8y/BE(H+MbQeThWmYq3t";

module.exports = encrypt;

function encrypt(message) {
    // the cipher function
    const cipher = crypto.createCipher(algorithm, password);

    // encrypt the message
    // input encoding
    // output encoding
    let encryptedData = cipher.update(message, "utf-8", "hex");

    encryptedData += cipher.final("hex");

    return encryptedData;
}