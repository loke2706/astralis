﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require("crypto");
const sendEmail = require('_helpers/send-email');
const db = require('_helpers/db');
const encrypt = require('_middleware/encryption');
const decrypt = require('_middleware/decryption');

module.exports = {
    authenticate,
    register,
    getAll,
    getById,
};

async function authenticate({ Email, Password }) {
    var mail = encrypt(Email);
    const account = await db.Account.findOne({ Email: mail });
    if (!bcrypt.compareSync(Password, account.Password)) {
        throw 'Email or Password is incorrect';
    }
    // authentication successful so generate jwt and refresh tokens
    const jwtToken = generateJwtToken(account);
    // return basic details and tokens
    account.Email = decrypt(account.Email);
    account.Username = decrypt(account.Username);
    account.Name = decrypt(account.Name);
    account.Mobile = decrypt(account.Mobile);
    return {
        ...basicDetails(account),
        jwtToken,
    };
}

async function register(params) {
    var mail = encrypt(params.Email);
    // validate
    if (await db.Account.findOne({ Email: mail })) {
        // send already registered error in Email to prevent account enumeration
        throw 'User Already registered.';
    }

    // create account object
    const account = new db.Account(params);
    account.Email = encrypt(account.Email);
    account.Username = encrypt(account.Username);
    account.Name = encrypt(account.Name);
    account.Mobile = encrypt(account.Mobile);
    // hash Password
    account.Password = hash(params.Password);

    // save account
    await account.save();
}

async function getAll() {
    const accounts = await db.Account.find();
    return accounts.map(function(x) {
        x.Email = decrypt(x.Email);
        x.Username = decrypt(x.Username);
        x.Name = decrypt(x.Name);
        x.Mobile = decrypt(x.Mobile);
        return basicDetails(x);
    });
}

async function getById(id) {
    const account = await getAccount(id);
    account.Email = decrypt(account.Email);
    account.Username = decrypt(account.Username);
    account.Name = decrypt(account.Name);
    account.Mobile = decrypt(account.Mobile);
    return basicDetails(account);
}

// helper functions

async function getAccount(id) {
    if (!db.isValidId(id)) throw 'Account not found';
    const account = await db.Account.findById(id);
    if (!account) throw 'Account not found';
    return account;
}

function hash(Password) {
    return bcrypt.hashSync(Password, 10);
}

function generateJwtToken(account) {
    // create a jwt token containing the account id that expires in 15 minutes
    return jwt.sign({ sub: account.id, id: account.id }, config.secret, { expiresIn: '15m' });
}

function basicDetails(account) {
    const { id, Email, Name, Username, Mobile, created, updated } = account;
    return { id, Email, Name, Username, Mobile, created, updated };
}