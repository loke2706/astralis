const db = require('_helpers/db');
const encrypt = require('_middleware/encryption');
const decrypt = require('_middleware/decryption');

module.exports = {
    createTransaction,
    getAll,
    getbyuserid,
    getbytransId,
    updatetransactionbyid,
    _delete,
    deletealltransactions
};


async function createTransaction(params) {
    var transactionid = encrypt(params.ID);
    // validate
    if (await db.Transaction.findOne({ ID: transactionid })) {
        // send already registered error in Email to prevent transaction enumeration
        throw 'Transaction Already completed with this Transaction Id.';
    }

    // create transaction object
    const transaction = new db.Transaction(params);

    transaction.ID = encrypt(transaction.ID);
    transaction.userId = encrypt(transaction.userId);
    transaction.AccountName = encrypt(transaction.AccountName);
    transaction.AccountNo = encrypt(transaction.AccountNo);
    transaction.BankIFSC = encrypt(transaction.BankIFSC);
    transaction.Amount = encrypt(transaction.Amount);
    transaction.Value = encrypt(transaction.Value);
    transaction.Remarks = encrypt(transaction.Remarks);

    // save transaction
    await transaction.save();
}

async function getAll() {
    const transactions = await db.Transaction.find();
    return transactions.map(function(x) {
        x.ID = decrypt(x.ID);
        x.userId = decrypt(x.userId);
        x.AccountName = decrypt(x.AccountName);
        x.AccountNo = decrypt(x.AccountNo);
        x.BankIFSC = decrypt(x.BankIFSC);
        x.Amount = decrypt(x.Amount);
        x.Value = decrypt(x.Value);
        x.Remarks = decrypt(x.Remarks);
        return basicDetails(x);
    });
}

async function getbyuserid(id) {
    var userid = encrypt(id);
    const transactions = await db.Transaction.find({ userId: userid });
    return transactions.map(function(x) {
        x.ID = decrypt(x.ID);
        x.userId = decrypt(x.userId);
        x.AccountName = decrypt(x.AccountName);
        x.AccountNo = decrypt(x.AccountNo);
        x.BankIFSC = decrypt(x.BankIFSC);
        x.Amount = decrypt(x.Amount);
        x.Value = decrypt(x.Value);
        x.Remarks = decrypt(x.Remarks);
        return basicDetails(x);
    });
}

async function getbytransId(id) {
    var transid = encrypt(id);
    const transactions = await db.Transaction.find({ ID: transid });
    if (!transactions) throw 'Transaction not found';
    if (transactions.length > 1) {
        return transactions.map(function(x) {
            x.ID = decrypt(x.ID);
            x.userId = decrypt(x.userId);
            x.AccountName = decrypt(x.AccountName);
            x.AccountNo = decrypt(x.AccountNo);
            x.BankIFSC = decrypt(x.BankIFSC);
            x.Amount = decrypt(x.Amount);
            x.Value = decrypt(x.Value);
            x.Remarks = decrypt(x.Remarks);
            return basicDetails(x);
        });
    } else if (transactions.length > 0) {
        const transaction = transactions[0];
        transaction.ID = decrypt(transaction.ID);
        transaction.userId = decrypt(transaction.userId);
        transaction.AccountName = decrypt(transaction.AccountName);
        transaction.AccountNo = decrypt(transaction.AccountNo);
        transaction.BankIFSC = decrypt(transaction.BankIFSC);
        transaction.Amount = decrypt(transaction.Amount);
        transaction.Value = decrypt(transaction.Value);
        transaction.Remarks = decrypt(transaction.Remarks);
        return basicDetails(transaction);
    }

}

async function updatetransactionbyid(id, params) {
    const transaction = await getAccount(id);
    params.AccountName = encrypt(params.AccountName);
    params.AccountNo = encrypt(params.AccountNo);
    params.Amount = encrypt(params.Amount);
    params.BankIFSC = encrypt(params.BankIFSC);
    params.Value = encrypt(params.Value);
    params.Remarks = encrypt(params.Remarks);
    // copy params to transaction and save
    Object.assign(transaction, params);
    transaction.updated = Date.now();
    await transaction.save();
    transaction.ID = decrypt(transaction.ID);
    transaction.userId = decrypt(transaction.userId);
    transaction.AccountName = decrypt(transaction.AccountName);
    transaction.AccountNo = decrypt(transaction.AccountNo);
    transaction.BankIFSC = decrypt(transaction.BankIFSC);
    transaction.Amount = decrypt(transaction.Amount);
    transaction.Value = decrypt(transaction.Value);
    transaction.Remarks = decrypt(transaction.Remarks);
    return basicDetails(transaction);
}

async function _delete(id) {
    const transaction = await getAccount(id);
    await transaction.remove();
}

async function deletealltransactions(id) {
    await db.Transaction.remove();
}


async function getAccount(id) {
    if (!db.isValidId(id)) throw 'Transaction not found';
    const transaction = await db.Transaction.findById(id);
    if (!transaction) throw 'Transaction not found';
    return transaction;
}

function basicDetails(transaction) {
    const { id, ID, userId, AccountName, AccountNo, BankIFSC, Amount, Value, Remarks, created } = transaction;
    return { id, ID, userId, AccountName, AccountNo, BankIFSC, Amount, Value, Remarks, created };
}